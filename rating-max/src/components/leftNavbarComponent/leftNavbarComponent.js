import ApiService from '../../services/api.services'
import router from '../../router/index'
export default {
  name: 'leftNavbarComponent',
  components: {},
  props: [],
  data () {
    return {
      data :[],
    }
  },
  computed: {

  },
  created(){
  },
  mounted () {  
    this.data = ApiService.getCategories()
  },
  methods: {
  }
}
