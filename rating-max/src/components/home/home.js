import  ApiService  from '../../services/api.services';
export default {
  name: 'headerComponent',
  components: {},
  props: [],
  data () {
    return {
      data:[]
    }
  },
  computed: {

  },
  created(){
  },
  mounted () {
    this.data = ApiService.getCategories()
  },
  methods: {
  }
}
