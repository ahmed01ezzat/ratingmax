import StarRating from 'vue-star-rating'
import ApiService from '../../services/api.services'

export default {
  name: 'customerServices',
  components: {StarRating},
  props:[],
  data () {
    return {
      data:[],
      miniHeart:1,
      borderWidth:2
    }
  },
  computed: {

  },
  created(){
  },
  mounted () {
    this.data = ApiService.getCustomerServices();
  },
  methods: {
  }
}
