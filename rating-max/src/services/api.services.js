import axios from 'axios'
const dummyArray = [
    {
        'imgUrl':require('../assets/training.png'),
        'title':'Training',
        'url': '/training'
    },
    {
        'imgUrl':require('../assets/2.png'),
        'title':'Customer Service',
        'url': '/customer-service'
    },
    {
        'imgUrl':require('../assets/3.png'),
        'title':'Operation',
        'url': '/operation'
    },
    {
        'imgUrl':require('../assets/3.png'),
        'title':'Classes',
        'url': '/classes'
    },
    {
        'imgUrl':require('../assets/4.png'),
        'title':'Medical',
        'url': '/medical'
    },
    {
        'imgUrl':require('../assets/5.png'),
        'title':'Staff Ratings',
        'url': '/staff-ratings'
    },
    {
        'imgUrl':require('../assets/6.png'),
        'title':'Complains',
        'url': '/complains'
    },
    {
        'imgUrl':require('../assets/3.png'),
        'title':'Spa',
        'url': '/spa'
    },
]

const customersServices = [
    {
        'imgUrl':require('../assets/Ell2.png'),
        'title':'Follow Up',
        'ratting': 2
    },
    {
        'imgUrl':require('../assets/Ell3.png'),
        'title':'Outlook',
        'ratting': 4
    },
    {
        'imgUrl':require('../assets/Ell2.png'),
        'title':'Training',
        'ratting': 3
    },
    {
        'imgUrl':require('../assets/Ell3.png'),
        'title':'Follow Up',
        'ratting': 1
    },
    
]
const ApiService = {

    init(baseURL) {
        axios.defaults.baseURL = baseURL;
    },

    setHeader() {
        // axios.defaults.headers.common["Authorization"] = `Bearer ${TokenService.getToken()}`
    },

    removeHeader() {
        axios.defaults.headers.common = {}
    },

    get(resource) {
        return axios.get(resource)
    },

    post(resource, data) {
        return axios.post(resource, data)
    },

    put(resource, data) {
        return axios.put(resource, data)
    },

    delete(resource) {
        return axios.delete(resource)
    },

    /**
     * Perform a custom Axios request.
     *
     * data is an object containing the following properties:
     *  - method
     *  - url
     *  - data ... request payload
     *  - auth (optional)
     *    - username
     *    - password
    **/
    customRequest(data) {
        return axios(data)
    },
    getCategories(){
    return dummyArray
    },
    getCustomerServices(){
        return customersServices;
    }
}

export default ApiService