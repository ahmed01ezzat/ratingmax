import Vue from 'vue'
import Router from 'vue-router'
import home from '../components/home/index.vue';
import customerServices from '../components/customerServices/index.vue'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import VueStar from 'vue-star'
Vue.component('VueStar', VueStar)
Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
        path: '/customer-service',
        name: 'customerServices',
        component: customerServices
      }
  ]
})